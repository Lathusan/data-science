import csv
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/");
mydb = myclient["GlobalLandTemp"]
mycol = mydb["Temperature"]

with open('GolobalLandTemperatures.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')

    thislist = []

    for row in readCSV:
        thisdict = {
            "dt" : row[0],
            "LandAverageTemperature" : row[1],
            "LandMaxTemperature" : row[2],
            "LandMaxTemperatureUncertainty" : row[3],
            "LandMinTemperature" : row[4],
            "LandMinTemperatureUncertainty" : row[5],
            "LandAndOceanAverageTemperature" : row[6],
            "LandAndOceanAverageTemperatureUncertainty" : row[7]
        }

        thislist.append(thisdict)
        mycol.insert_one(thisdict)
        
#f = open('GlobalLandTemperatures.csv')

#csv_f = csv.reader(f)

#attendees1 = []

#for row in csv_f:
#    attendees1.append(row[0])
#    print (attendees1)

#f.close ()

#print (len(attendees1))