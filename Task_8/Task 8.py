#!/usr/bin/env python
# coding: utf-8

# In[76]:


from sklearn import tree
import pandas as pd
import pydotplus
from IPython.display import Image


# In[77]:


read_annual = pd.read_csv('Annual.csv')
read_annual


# In[78]:


dummi_data = read_annual.iloc[:, 0:4].values
dummi_data


# In[79]:


read_annual = read_annual[['Source', 'Year','Mean','Temperature','Weather']]
read_annual


# In[80]:


one_hot_data = pd.get_dummies(read_annual)
one_hot_data


# In[81]:


clf = tree.DecisionTreeClassifier()
clf_train = clf.fit(one_hot_data, read_annual['Weather'])
clf_train = clf.fit(one_hot_data, read_annual['Temperature'])


# In[82]:


dot_data = tree.export_graphviz(clf_train, out_file=None, feature_names=list(one_hot_data.columns.values), 
                                class_names=['Cold', 'Overheating', 'Hot', 'Normal heat', 'Normal cold', 
                                             'Excessive cold'],
                                rounded=True, filled=True)

graph = pydotplus.graph_from_dot_data(dot_data)
Image(graph.create_png())

