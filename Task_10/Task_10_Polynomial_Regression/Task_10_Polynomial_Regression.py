#!/usr/bin/env python
# coding: utf-8

# In[35]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


# In[36]:


csvdata = pd.read_csv('GolobalLandTemperatures.csv')
csvdata


# In[37]:


X = csvdata.iloc[:,0:1].values
y = csvdata.iloc[:,1].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)


# In[38]:


X_train, X_test, y_train, y_test


# In[39]:


from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)


# In[40]:


def show_linear():
    plt.scatter(X, y, color='blue')
    plt.plot(X, lin_reg.predict(X), color='green')
    plt.title('Linear Regression for Golobal Land Temperatures.')
    plt.xlabel('Year')
    plt.ylabel('***********************************************')
    plt.show()
    return
show_linear() 


# In[41]:


lin_reg.predict([[5.5]])


# In[42]:


from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree=4)
X_poly = poly_reg.fit_transform(X)
pol_reg = LinearRegression()
pol_reg.fit(X_poly, y)


# In[43]:


def show_polymonial():
    plt.scatter(X, y, color='red')
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
    plt.title('Linear Regression for Golobal Land Temperatures.')
    plt.xlabel('Year')
    plt.ylabel('******************************************************')
    plt.show()
    return

show_polymonial()


# In[44]:


pol_reg.predict(poly_reg.fit_transform([[5.5]]))


# In[45]:


predicted = {}
for i in range(1,31):
    yhat = -53.21 + 0.0371 * (2000 + i)
    predicted[2020 + i] = yhat


# In[46]:


predicted

