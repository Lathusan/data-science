#!/usr/bin/env python
# coding: utf-8

# In[135]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm


# In[136]:


data = pd.read_csv('GolobalLandTemperatures.csv')


# In[137]:


data


# In[138]:


data.describe()


# In[139]:


y = data['Year']
x1 = data['LandMinTemperature']


# In[140]:


plt.scatter(x1,y)
yhat = 0.8*x1 + 10
fig = plt.plot(x1,yhat, lw=4, c='orange',label='regression line')
plt.xlabel('LandMinTemperature',fontsize=10)
plt.ylabel('Year',fontsize=10)
plt.show()


# In[141]:


x =  sm.add_constant(x1)
results = sm.OLS(x,y).fit()
results.summary()

